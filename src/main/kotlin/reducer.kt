fun reducer(): (state: AppState, actions: Action) -> AppState {
     return { state: AppState, action: Action ->
         when(action.type) {
             ActionTypes.ADD_NAMES -> {
                 when (action.payload.get("names")) {
                     is Array<*> -> state.names.addAll(action.payload.get("names") as Array<String>)
                     else -> console.error("BAD PAYLOAD FOR " + action.toString())
                 }
             }
             else -> state
         }
         state
     }
}