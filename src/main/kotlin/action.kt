enum class ActionTypes {
    ADD_NAMES,
}

interface Action {
    val type: ActionTypes
    val payload: Map<String, Any>
}


data class AddNamesAction(val names: Array<String> = arrayOf()): Action {
    override val type = ActionTypes.ADD_NAMES
    override val payload: Map<String, Array<String>>
        get() = mapOf("names" to names)
}

