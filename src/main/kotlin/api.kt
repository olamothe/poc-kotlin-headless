import kotlin.browser.window

class API {
    @JsName("kotlinStore")
    var kotlinStore: Store
    var store: dynamic

    init {
        kotlinStore =Store()
        store = kotlinStore.reduxStore
    }

    @JsName("fetchNames")
    public fun fetchNames() {
        window.fetch("https://dart.dev/f/portmanteaux.json").then { res ->
            res.json().then {
                kotlinStore.dispatch(AddNamesAction(it as Array<String>))
            }
        }
    }
}
