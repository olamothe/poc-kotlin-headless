data class AppState(val names: MutableList<String> = mutableListOf())


class Store {
    var reduxStore = createStore(reducer(), AppState())

    fun dispatch(action: Action) {
        reduxStore.dispatch(plainJSObject {
            type = action.type
            payload = action.payload
        })
    }

    fun getState(): dynamic {
        return reduxStore.getState()
    }

    private fun plainJSObject(init: dynamic.() -> Unit): dynamic {
        val o = js("{}")
        init(o)
        return o
    }
}

